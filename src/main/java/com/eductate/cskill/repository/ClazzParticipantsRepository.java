package com.eductate.cskill.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.eductate.cskill.model.ClazzParticipants;

@Repository
public interface ClazzParticipantsRepository extends JpaRepository<ClazzParticipants, Long> {
	
	List<ClazzParticipants> findByClazzKey(String clazzKey);

}
