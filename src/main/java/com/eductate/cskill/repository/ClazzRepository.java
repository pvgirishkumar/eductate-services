package com.eductate.cskill.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.eductate.cskill.model.Clazz;


@Repository
public interface ClazzRepository extends JpaRepository<Clazz, Long> {

	Optional<Clazz> findByClazzKey(String clazzKey);
	
//	List<Clazz> findAllClazzByCourseKey(String courseKey);
//	
//	List<Clazz> findAllClazzByBatchKey(String batchKey);
}
