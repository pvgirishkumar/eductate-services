package com.eductate.cskill.dto;

import java.util.Date;

import javax.persistence.Column;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ClazzDto extends BaseDto{
	
	private static final long serialVersionUID = 1L;
	
	private String clazzKey;
	
	private String title;
	
	private String description;
	
	private String previewImg;
	
	private String previewVideo;
	
	private Date startTime;
	
	private Date endTime;
	
	private String status;
	
	private String categoryKey;
	
	private String categoryTitle;
	
	private String userKey;

}
