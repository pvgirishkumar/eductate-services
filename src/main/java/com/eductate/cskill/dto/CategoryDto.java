package com.eductate.cskill.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CategoryDto extends BaseDto {

	private static final long serialVersionUID = 1L;
	
	private String categoryKey;
	
	private String title;

	private String description;
	
	private String status;
}
