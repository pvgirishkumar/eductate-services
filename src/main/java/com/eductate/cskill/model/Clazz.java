package com.eductate.cskill.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import lombok.Getter;
import lombok.Setter;


@Setter
@Getter
@Entity
@Table(name = "clazz")
public class Clazz extends BaseObject{
	
	private static final long serialVersionUID = APP_SERIAL_ID;
	
	@Column(name = "clazz_key", nullable = true, unique = true)
	private String clazzKey;
	
	@Column(name = "title")
	private String title;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "preview_img")
	private String previewImg;
	
	@Column(name = "preview_video")
	private String previewVideo;
	
	@Column(name = "start_time")
	private Date startTime;
	
	@Column(name = "end_time")
	private Date endTime;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "category_key", nullable = true)
	private String categoryKey;
	
	@Column(name = "category_title", nullable = true)
	private String categoryTitle;
	
	@Column(name = "user_key", nullable = false)
	private String userKey;
	
	//private clazzType

}
