package com.eductate.cskill.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "user_preferences")
public class UserPreferences extends BaseObject {
	
private static final long serialVersionUID = APP_SERIAL_ID;
	
	@Column(name = "user_key", nullable = false)
	private String userKey;
	
	@Column(name = "category_key", nullable = false)
	private String categoryKey;

}
