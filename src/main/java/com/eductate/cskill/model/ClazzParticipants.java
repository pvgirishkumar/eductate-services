package com.eductate.cskill.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "clazz_participants")
public class ClazzParticipants extends BaseObject {

	private static final long serialVersionUID = APP_SERIAL_ID;
	
	@Column(name = "user_key", nullable = false)
	private String userKey;
	
	@Column(name = "clazz_key", nullable = false)
	private String clazzKey;
	
	@Column(name = "status") 
	private String status; //booked,approved,disapporved, joined, completed, cancelled --> Enum
	
		
}
