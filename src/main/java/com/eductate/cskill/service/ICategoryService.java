package com.eductate.cskill.service;

import java.util.List;

import com.eductate.cskill.dto.CategoryDto;


public interface ICategoryService {

	CategoryDto createCategory(CategoryDto cateDto);
	
	CategoryDto updateCategory(CategoryDto cateDto);
	
	CategoryDto getCategory(String categoryKey);
	
	List<CategoryDto> getAllCategory();
	
}
