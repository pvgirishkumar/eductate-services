package com.eductate.cskill.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eductate.cskill.dto.BaseDto;
import com.eductate.cskill.dto.ClazzDto;
import com.eductate.cskill.model.Clazz;
import com.eductate.cskill.repository.ClazzRepository;
import com.eductate.cskill.service.IClazzService;
import com.eductate.cskill.utils.BeanConverter;
import com.eductate.cskill.utils.CommonUtilities;


@Service
@Transactional(readOnly = true)
public class ClazzService implements IClazzService{
	
	@Autowired
	private ClazzRepository clazzRepo;

	@Override
	@Transactional
	public ClazzDto createClazz(ClazzDto source) {
		Clazz entity = new Clazz();
		BeanUtils.copyProperties(source, entity);
		clazzRepo.save(entity);
		entity.setClazzKey(CommonUtilities.getUniqueId(16, entity.getId()));
		source.setClazzKey(entity.getClazzKey());
		return source;
	}

	@Override
	@Transactional
	public ClazzDto updateClazz(ClazzDto source) {
		Optional<Clazz> entity = clazzRepo.findByClazzKey(source.getClazzKey());
		if(entity.isPresent()) {
			Clazz target = entity.get();
			BeanUtils.copyProperties(source, target, "id");
			clazzRepo.save(target);
		}
		return source;
	}

	@Override
	public ClazzDto getClazz(String clazzKey) {
		Optional<Clazz> entity = clazzRepo.findByClazzKey(clazzKey);
		Clazz target = null;
		if(entity.isPresent()) {
			target = entity.get();
		}
		return BeanConverter.toClazzDto(target);
	}

//	@Override
//	public List<ClazzDto> getAllClazzByCourseKey(String courseKey) {
//		List<Clazz> lists = clazzRepo.findAllClazzByCourseKey(courseKey);
//		if(lists.size() > 0) {
//			List<ClazzDto> results = lists.stream().map(m -> BeanConverter.toClazzDto(m)).collect(Collectors.toList());
//			return results;
//		}
//		return null;
//	}
//
//	@Override
//	public List<ClazzDto> getAllClazzByBatchKey(String batchKey) {
//		List<Clazz> lists = clazzRepo.findAllClazzByBatchKey(batchKey);
//		if(lists.size() > 0) {
//			List<ClazzDto> results = lists.stream().map(m -> BeanConverter.toClazzDto(m)).collect(Collectors.toList());
//			return results;
//		}
//		return null;
//	}

	@Override
	public BaseDto deleteClazz(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
