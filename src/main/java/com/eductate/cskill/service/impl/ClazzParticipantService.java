package com.eductate.cskill.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eductate.cskill.model.Category;
import com.eductate.cskill.model.ClazzParticipants;
import com.eductate.cskill.repository.ClazzParticipantsRepository;

@Service
@Transactional(readOnly = true)
public class ClazzParticipantService {
	
	@Autowired
	private ClazzParticipantsRepository clazzPartiRepo; 
	
	@Transactional
	public ClazzParticipants createORupdateClazzParticipant(ClazzParticipants source) {
		clazzPartiRepo.save(source);
		return source;
	}
	
	public List<ClazzParticipants> getClazzparticipantsByClazzKey(String clazzKey){
		List<ClazzParticipants> lists = clazzPartiRepo.findByClazzKey(clazzKey);
		return lists;
	}

}
