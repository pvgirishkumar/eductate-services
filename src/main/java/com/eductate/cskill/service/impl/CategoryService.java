package com.eductate.cskill.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eductate.cskill.dto.CategoryDto;
import com.eductate.cskill.model.Category;
import com.eductate.cskill.repository.CategoryRepository;
import com.eductate.cskill.service.ICategoryService;
import com.eductate.cskill.utils.BeanConverter;
import com.eductate.cskill.utils.CommonUtilities;


@Service
@Transactional(readOnly = true)
public class CategoryService implements ICategoryService {
	
	@Autowired
	private CategoryRepository cateRepo;

	@Override
	@Transactional
	public CategoryDto createCategory(CategoryDto cateDto) {
		Category entity = new Category();
		BeanUtils.copyProperties(cateDto, entity);
		cateRepo.save(entity);
		entity.setCategoryKey(CommonUtilities.getUniqueId(16, entity.getId()));
		cateDto.setCategoryKey(entity.getCategoryKey());
		return cateDto;
	}

	@Override
	@Transactional
	public CategoryDto updateCategory(CategoryDto cateDto) {
		Optional<Category> entity = cateRepo.findByCategoryKey(cateDto.getCategoryKey());
		if(entity.isPresent()) {
			Category target = entity.get();
			BeanUtils.copyProperties(cateDto, target, "id");
			cateRepo.save(target);
		}
		return cateDto;
	}

	@Override
	public CategoryDto getCategory(String categoryKey) {
		CategoryDto target = null;
		Optional<Category> entity = cateRepo.findByCategoryKey(categoryKey);
		if(entity.isPresent()) {
			target = new CategoryDto();
			BeanUtils.copyProperties(entity.get(), target);
		}
		return target;
	}

	@Override
	public List<CategoryDto> getAllCategory() {
		List<Category> lists = cateRepo.findAll();
		if(lists.size() > 0) {
			List<CategoryDto> results = lists.stream().map(m -> BeanConverter.toCategoryDto(m)).collect(Collectors.toList());
			return results;
		}
		return null;
	}


}
