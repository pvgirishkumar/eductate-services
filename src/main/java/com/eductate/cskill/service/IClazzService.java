package com.eductate.cskill.service;

import java.util.List;

import com.eductate.cskill.dto.BaseDto;
import com.eductate.cskill.dto.ClazzDto;

public interface IClazzService {

	ClazzDto createClazz(ClazzDto source);
	
	ClazzDto updateClazz(ClazzDto source);
	
	ClazzDto getClazz(String clazzKey);
	
	BaseDto deleteClazz(Long id);
}
