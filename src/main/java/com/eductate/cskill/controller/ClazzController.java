package com.eductate.cskill.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eductate.cskill.dto.ClazzDto;
import com.eductate.cskill.service.IClazzService;
import com.eductate.cskill.utils.RequestPath;


@RestController
@RequestMapping(RequestPath.V1 + RequestPath.CLAZZ)
public class ClazzController {

	@Autowired
	private IClazzService classService;
	
	@PostMapping
	public ClazzDto createClazz(@RequestBody ClazzDto source) {
		return classService.createClazz(source);
	}
	
	@PutMapping
	public ClazzDto updateClazz(@RequestBody ClazzDto clazz) {
		return classService.updateClazz(clazz);
	}
	
	@GetMapping("{clazzKey}")
	public ClazzDto getClazz(@PathVariable(name="clazzKey") String clazzKey) {
		return classService.getClazz(clazzKey);
	}
}
