package com.eductate.cskill.utils;

import com.eductate.cskill.dto.BaseDto;
import com.eductate.cskill.dto.CategoryDto;
import com.eductate.cskill.dto.ClazzDto;
import com.eductate.cskill.model.BaseObject;
import com.eductate.cskill.model.Category;
import com.eductate.cskill.model.Clazz;

public class BeanConverter {

	private BeanConverter() {

	}
	
	private static void toBaseDto(BaseObject source, BaseDto target) {
		if(source != null && target != null) {
			target.setId(source.getId());
			target.setLastUpdatedOn(source.getLastUpdatedOn());
			target.setCreatedOn(source.getCreatedOn());
		}
	}
	
	public static ClazzDto toClazzDto(Clazz source) {
		ClazzDto target = null;
		if(source != null) {
			target = new ClazzDto();
			toBaseDto(source, target);
			target.setClazzKey(source.getClazzKey());
			target.setTitle(source.getTitle());
			target.setDescription(source.getDescription());
			target.setPreviewImg(source.getPreviewImg());
			target.setPreviewVideo(source.getPreviewVideo());
			target.setStartTime(source.getStartTime());
			target.setEndTime(source.getEndTime());
			target.setStatus(source.getStatus());
			target.setCategoryKey(source.getCategoryKey());
			target.setCategoryTitle(source.getCategoryTitle());
			target.setUserKey(source.getUserKey());
		}
		return target;
	}
	
	public static CategoryDto toCategoryDto(Category source) {
		CategoryDto target = null;
		if(source != null) {
			target =  new CategoryDto();
			toBaseDto(source, target);
			target.setCategoryKey(source.getCategoryKey());
			target.setDescription(source.getDescription());
			target.setTitle(source.getTitle());
			target.setStatus(source.getStatus());
		}
		return target;
	}
}
